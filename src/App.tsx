// import a module
// import React from 'react';
// import logo from './logo.svg';

// // creates a <style> tag
// import './App.css';

// COMPONENTS
// import NetNinja from './components/net-ninja/NetNinja'
// import TicTacToe from './components/TicTacToe'
// import TypeScript from './components/ts-basics/TypeScript'
// import HighOrder from './components/higher-order-component/HighOrder'
// import MyHook from './components/hooks/custom-hooks/MyHook'
// import UseRef from './components/hooks/useRef/UseRef'
// import IncDec from './components/hooks/useReducer/IncDec'
// import ShoppingCart from "./components/shopping-cart/ShoppingCart";
// import Todos from "./components/kf-test/Todos";
import WdsAuth from "./components/wds-auth/WdsAuth";

function App() {
  return (
    <div className="m-12">
      {/* <NetNinja /> */}
      {/* <TicTacToe /> */}
      {/* <TypeScript text="woriking..." /> */}
      {/* <HighOrder /> */}
      {/* <MyHook /> */}
      {/* <UseRef /> */}
      {/* <IncDec /> */}
      {/* <ShoppingCart /> */}
      {/* <Todos /> */}
      <WdsAuth />

      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    </div>
  );
}

export default App;
