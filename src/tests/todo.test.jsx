import { render, screen, cleanup } from "@testing-library/react";
import renderer from "react-test-renderer";
import Todos from "./../components/kf-test/Todos";
import Todo from "./../components/kf-test/Todo";

// fresh every test run
afterEach(() => cleanup());

test("render todos component", () => {
  // render a component
  render(<Todos />);
  // get attribute "data-testid"
  const el = screen.getByTestId("todos");

  // check if element exist in document
  expect(el).toBeInTheDocument();
});

test("render completed todo component", () => {
  // dummy object
  const todo = { id: 1, title: "title 1", completed: true };

  // render a component w/ props
  render(<Todo id={todo.id} title={todo.title} completed={todo.completed} />);

  // get attribute "data-testid"
  const el = screen.getByTestId("todo-1");

  // check if element exists in document
  expect(el).toBeInTheDocument();

  // check if text exists in element
  expect(el).toHaveTextContent("title 1");

  // // el contains an element of <h2>
  // expect(el).toContainHTML("<h2>");

  // // el doesnt contain an element of <h2>
  // expect(el).not.toContainHTML("<h2");
});

test("render not completed todo component", () => {
  const todo = { id: 2, title: "title 2", completed: false };
  render(<Todo id={todo.id} title={todo.title} completed={todo.completed} />);
});

test("match snapshot", () => {
  const todo = { id: 2, title: "title 2", completed: false };
  const tree = renderer.create(
    <Todo id={todo.id} title={todo.title} completed={todo.completed} />
  );
  console.log(tree);
});
