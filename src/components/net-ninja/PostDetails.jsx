import { useParams } from "react-router-dom";
import useFetch from "./useFetch";
import Post from "./Post";

const PostDetails = () => {
  const { id } = useParams();
  const { data: post, isLoading, error } = useFetch(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  );
  console.log(post);
  return (
    <div>
      {isLoading && <p>Loading...</p>}
      {error && <p>{error}</p>}
      {post && <Post title={post.title} body={post.body} />}
    </div>
  );
};

export default PostDetails;
