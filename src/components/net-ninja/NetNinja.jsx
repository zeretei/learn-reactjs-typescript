import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// import components
import Navbar from "./Navbar";
import Home from "./Home";
import Posts from "./Posts";
import PostDetails from "./PostDetails";
import Create from "./Create";
import NotFound from "./NotFound";

const NetNinja = () => {
  // variable
  const title = "Net Ninja Component";

  // inline style
  const inlineStyle = {
    color: "#4c4cff",
    textDecoration: "underline",
  };

  // create a function
  const handleClick = (name, e) => console.log(`Hello, ${name}`, e.target);

  return (
    <Router>
      <article className="space-y-5">
        <section>
          <header>
            {/* use component */}
            <Navbar />
          </header>
        </section>
        <hr />

        <Switch>
          <Route exact path="/">
            <section>
              <Home />
            </section>
          </Route>

          <Route exact path="/etc">
            <section className="space-y-5">
              {/* render variable */}
              <h2 className="text-4xl font-bold">{title}</h2>

              {/* inline js exression */}
              <p>{Math.floor(Math.random() * 100) + 1}</p>

              {/* set inline style */}
              <p style={inlineStyle}>inline style</p>

              {/* 
                call function
                e: element event 
                */}
              <button
                onClick={(e) => handleClick("John", e)}
                className="border px-3 py-2 rounded hover:bg-gray-200"
              >
                Click Me!
              </button>
            </section>

            <hr />
          </Route>

          <Route exact path="/posts">
            <section>
              <h2 className="text-4xl font-bold">Request</h2>
              <Posts />
            </section>
          </Route>

          <Route exact path="/blog/:id">
            <section>
              <h2>Post Details</h2>
              <PostDetails />
            </section>
          </Route>

          <Route exact path="/create">
            <Create />
          </Route>

          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </article>
    </Router>
  );
};

export default NetNinja;
