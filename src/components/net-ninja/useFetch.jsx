import { useState, useEffect } from "react";

const useFetch = (url) => {
  // create a state for posts
  const [data, setData] = useState(null);

  //   create a state for loading and error
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    // about 1 or more request
    const abortController = new AbortController();

    // set signal property of fetch()
    const signal = { signal: abortController.signal };

    // use async function to fetch data
    const fetchData = async (url, signal) => {
      // use try...catch to handle error
      try {
        // request using fetch api
        const req = await fetch(url, signal);

        // check request errors
        if (!req.ok) throw Error("Request Failed");

        // get the response data as json
        const res = await req.json();

        // set posts state
        setData(res);

        // set posts state to false
        setIsLoading(false);

        setError(null);
      } catch (err) {
        // catch the about error
        if (err.name === "AbortError") {
          console.log("Fetch Aborted");
        } else {
          setIsLoading(false);
          setError(err.message);
        }
      }
    };

    // call async function
    fetchData(url, signal);

    return () => abortController.abort();
  }, [url]);

  return { data, isLoading, error };
};

export default useFetch;
