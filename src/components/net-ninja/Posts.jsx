import BlogList from "./BlogList";
import useFetch from "./useFetch";

const Posts = () => {
  // data as 'posts'
  const { data: posts, isLoading } = useFetch(
    "https://jsonplaceholder.typicode.com/posts"
  );

  // const handleDelete = (id) => {
  //   // get all blogs except id
  //   const newPosts = posts.filter((post) => post.id !== id);

  //   // set new  filtered blogs
  //   setData(newPosts);
  // };

  return (
    <div>
      {/* conditional loading message */}
      {isLoading && <p>Loading...</p>}

      {posts && <BlogList blogs={posts} title="All Blogs" />}
    </div>
  );
};

export default Posts;
