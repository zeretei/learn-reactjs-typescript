import { useState, useEffect } from "react";
import BlogList from "./BlogList";

// set render counter
let counter = 0;

const Home = () => {
  // use react state
  const [name, setName] = useState("John");
  const [age, setAge] = useState(5);

  // create a list
  const [blogs, setBlogs] = useState([
    { id: 1, title: "blog 1", body: "lorem ipsum...", author: "John" },
    { id: 2, title: "blog 2", body: "lorem ipsum...", author: "jane" },
    { id: 3, title: "blog 3", body: "lorem ipsum...", author: "sam" },
  ]);

  // update a state
  const changeName = () => {
    setName("Jane");
  };

  // delete a blog
  const handleDelete = (id) => {
    // get all blogs except id
    const newBlogs = blogs.filter((blog) => blog.id !== id);

    // set new  filtered blogs
    setBlogs(newBlogs);
  };
  // use react useEffect
  // []: set dependency
  useEffect(() => {
    console.log("use effect ran");

    // set state will run only if the state changed
    console.log(name);

    console.log(age);
  }, [name, age]);

  // count how many times the component rendered
  counter += 1;

  return (
    <article className="space-y-5">
      <h2 className="text-4xl font-bold">home component</h2>
      <section className="space-y-5">
        {/* render state */}
        <p>render: {counter}</p>
        <p>
          {name} is {age} years old
        </p>

        {/* call changeName() function */}
        <button
          onClick={changeName}
          className="border px-3 py-2 mr-4 rounded hover:bg-gray-200"
        >
          Change Name
        </button>
        <button
          onClick={() => setAge(age + 1)}
          className="border px-3 py-2 rounded hover:bg-gray-200"
        >
          Change Age
        </button>
      </section>

      <section>
        {/* 
          set pops
          handleDelete: function prop
         */}
        <BlogList blogs={blogs} title="All Blogs" handleDelete={handleDelete} />

        {/* filtered blogs */}
        <BlogList
          blogs={blogs.filter((blog) => blog.author === "John")}
          title="John's Blogs"
          handleDelete={handleDelete}
        />
      </section>
    </article>
  );
};

export default Home;
