import { useState } from "react";
import { useHistory } from "react-router-dom";

const Create = () => {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [author, setAuthor] = useState("John");
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();

    const createPost = async () => {
      const post = { title, body, author };
      let req = await fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(post),
      });

      console.log(req);
      setIsLoading(true);

      setTimeout(() => {
        // history.go(-1);
        history.push("/posts");
      }, 1000);
    };
    createPost();
  };

  return (
    <div className="w-1/3 mx-auto text-left shadow p-5 rounded bg-blue-50">
      <form onSubmit={handleSubmit}>
        <div className="mb-6">
          <label className="block mb-1 text-sm">Title</label>
          <input
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            className="w-full border px-4 py-2 rounded focus:border-blue-500 focus:shadow-outline outline-none"
          />
        </div>

        <div className="mb-6">
          <label className="block mb-1 text-sm">Body</label>
          <textarea
            value={body}
            onChange={(e) => setBody(e.target.value)}
            className="w-full border px-4 py-2 rounded focus:border-blue-500 focus:shadow-outline outline-none"
          />
        </div>

        <div className="relative mb-6">
          <label className="block mb-1 text-sm">Author</label>
          <select
            vallue={author}
            onChange={(e) => setAuthor(e.target.value)}
            className="w-full border px-4 pr-8 py-2 rounded focus:border-blue-500 focus:shadow-outline outline-none appearance-none"
          >
            <option>John</option>
            <option>Jane</option>
            <option>Sam</option>
          </select>
        </div>
        <div className="mb-6">
          <button className="w-full py-2 px-3 text-white bg-blue-400 rounded">
            {isLoading ? "loading..." : "Submit"}
          </button>
        </div>
      </form>
    </div>
  );
};

export default Create;
