const Post = ({ title, body }) => {
  return (
    <div className="w-1/3 mx-auto">
      <h2 className="text-lg font-bold">{title}</h2>
      <p className="text-sm text-gray-700">{body}</p>
    </div>
  );
};

export default Post;
