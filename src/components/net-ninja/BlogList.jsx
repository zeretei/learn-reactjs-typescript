import { useHistory } from "react-router-dom";

import { Link } from "react-router-dom";

// destructure props
const BlogList = ({ blogs, title }) => {
  const history = useHistory();

  const handleDelete = (id) => {
    const deleteBlog = async (id) => {
      let req = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${id}`,
        {
          method: "DELETE",
        }
      );
      console.log(req);
      setTimeout(() => {
        // history.go(-1);
        history.push("/");
      }, 1000);
    };
    deleteBlog(id);
  };

  return (
    <div className="mb-4">
      <h2 className="text-2xl font-bold">{title}</h2>
      <div className="flex justify-center flex-wrap gap-5">
        {/* loop through list */}
        {blogs.slice(1, 10).map((blog) => {
          return (
            // key: unique identifier for list
            <div key={blog.id} className="w-1/4 p-2 bg-blue-50 shadow rounded">
              <div className="h-full flex flex-col justify-between">
                <Link to={`/blog/${blog.id}`}>
                  <h2 className="text-lg font-bold hover:underline">
                    {blog.title}
                  </h2>
                </Link>
                <p className="my-2 text-xs text-gray-700">{blog.body}</p>

                {/* use props function */}
                <button
                  onClick={() => handleDelete(blog.id)}
                  className="p-1 text-sm text-white bg-red-400 rounded"
                >
                  Delete Blog
                </button>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default BlogList;
