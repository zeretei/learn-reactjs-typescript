import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav>
      <h2 className="text-4xl font-bold">Navbar Component</h2>
      <div className="flex justify-center gap-5 text-blue-400 underline">
        <Link to="/" className="hover:text-purple-700">
          Home
        </Link>
        <Link to="/create" className="hover:text-purple-700">
          New Blog
        </Link>
        <Link to="/posts" className="hover:text-purple-700">
          Posts
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;
