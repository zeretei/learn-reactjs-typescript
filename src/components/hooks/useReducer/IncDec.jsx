import { useReducer } from "react";

const IncDec = () => {
  const ACTIONS = {
    INCREMENT: "increment",
    DECREMENT: "decrement",
  };

  // reducer function
  const reducer = (state, action) => {
    // actions to dispact
    switch (action.type) {
      case ACTIONS.INCREMENT:
        if (state.count > 9) return state;
        return { count: state.count + 1 };

      case ACTIONS.DECREMENT:
        if (state.count < -9) return state;
        return { count: state.count - 1 };

      default:
        return state;
    }
  };

  // useReducer(callback, state)
  // [state, action]
  const [state, dispatch] = useReducer(reducer, { count: 0 });

  const increment = () => {
    // dispatch an event
    dispatch({ type: ACTIONS.INCREMENT });
  };

  const decrement = () => {
    dispatch({ type: ACTIONS.DECREMENT });
  };

  return (
    <div className="text-center">
      <h2 className="text-4xl font-bold mb-6">Max of 10 and Min of -10</h2>
      <button onClick={increment} className="py-2 px-3 bg-blue-400 rounded">
        Increment
      </button>
      <span className="mx-4 p-2 text-xl font-bold">{state.count}</span>
      <button onClick={decrement} className="py-2 px-3 bg-red-400 rounded">
        Decrement
      </button>
    </div>
  );
};

export default IncDec;
