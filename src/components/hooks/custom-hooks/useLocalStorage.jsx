import { useState, useEffect } from "react";

const getLocalStorage = (key, initValue) => {
  // get value from local storage then parse JSON string
  const savedValue = JSON.parse(localStorage.getItem(key));

  // if savedValue exists return savedValue
  if (savedValue) return savedValue;

  // if initValue is a function return initValue()
  if (initValue instanceof Function) return initValue();

  // return initValue if above statement fails
  return initValue;
};

const useLocalStorage = (key, initValue) => {
  // use custom effect defined parameters
  const [data, setData] = useState(() => getLocalStorage(key, initValue));

  // update when data changes
  useEffect(() => {
    // set an item to local storage
    localStorage.setItem(key, JSON.stringify(data));

    // watch data dependency
  }, [data]);

  // return an array for custom hook to use
  return [data, setData];
};

export default useLocalStorage;
