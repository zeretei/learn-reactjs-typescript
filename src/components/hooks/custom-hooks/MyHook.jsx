// import custom hooks
import useLocalStorage from "./useLocalStorage";
import useUpdateLogger from './useUpdateLogger'

const MyHook = () => {
  // use custom hook
  const [value, setValue] = useLocalStorage("name", "");

  // custom hooks
  useUpdateLogger(value);
  
  return (
    <div>
      <div className="useLocalStorage">
        <input
          type="text"
          value={value}
          onChange={(e) => setValue(e.target.value)}
          className="py-2 px-3 border rounded"
        />
      </div>
    </div>
  );
};

export default MyHook;
