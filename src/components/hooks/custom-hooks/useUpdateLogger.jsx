import { useEffect } from "react";

// create a custom hook
const useUpdateLogger = (value) => {
  
  // log value whenever the value changes
  useEffect(() => {
    console.log(value);
  }, [value]);
};

export default useUpdateLogger;
