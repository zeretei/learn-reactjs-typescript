import { useRef, useEffect, useState } from "react";

const UseRef = () => {
  const [name, setName] = useState("");

  // set use ref
  const inputRef = useRef();
  const divRef = useRef();
  const prevRef = useRef();

  function click() {
    // #input3
    inputRef.current.focus();
  }
  useEffect(() => {
    // { current: input#input3 }
    console.log(inputRef);
    console.log(divRef);

    // preveious state name value
    prevRef.current = name;
  }, [name]);
  return (
    <div ref={divRef}>
      <h2>use ref</h2>
      <input
        id="input1"
        // set ref
        ref={inputRef}
        className="py-2 px-3 border rounded"
        type="text"
      />
      <input
        id="input2"
        // set ref
        ref={inputRef}
        className="py-2 px-3 border rounded"
        type="text"
      />
      <input
        id="input3"
        // set ref
        ref={inputRef}
        className="py-2 px-3 border rounded"
        type="text"
      />
      <button onClick={click}>focus</button>
    </div>
  );
};

export default UseRef;
