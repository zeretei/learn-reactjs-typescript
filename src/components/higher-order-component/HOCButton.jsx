import React from "react";

// accept a component and a starting counter value
const HOCButton = (Component, start = 0) => {
  // return a class component
  return class extends React.Component {
    constructor(props) {
      super(props);

      // set state counter
      this.state = {
        counter: start,
      };
    }

    // increment state counter
    handleClick = () => {
      this.setState({
        counter: this.state.counter + 1,
      });
    };

    render() {
      // return component
      return (
        <div>
          <Component
            // set props
            counter={this.state.counter}
            handleClick={this.handleClick}
          />
        </div>
      );
    }
  };
};

export default HOCButton;
