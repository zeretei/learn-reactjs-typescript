import { Component } from "react";

// import Higher Order Component
import HOCButton from "./HOCButton";

// create a class component
class Like extends Component {
  render() {
    return (
      <div>
        <p>
          {/* print print counter  */}
          likes: <span>{this.props.counter}</span>
        </p>
        <button
          // call props handleClick method
          onClick={this.props.handleClick}
          className="py-2 px-3 bg-blue-400 rounded hover:bg-blue-500"
        >
          like
        </button>
      </div>
    );
  }
}

// use HOC
const HOCLike = HOCButton(Like, 5);

export default HOCLike;
