import { Component } from "react";

// import Higher Order Component
import HOCButton from "./HOCButton";

// create a class component
class Comment extends Component {
  render() {
    return (
      <div>
        <p>
          {/* print print counter  */}
          comments: <span>{this.props.counter}</span>
        </p>
        <button
          // call props handleClick method
          onClick={this.props.handleClick}
          className="py-2 px-3 bg-yellow-400 rounded hover:bg-yellow-500"
        >
          comment
        </button>
      </div>
    );
  }
}

// use HOC
const HOCComment = HOCButton(Comment);

export default HOCComment;
