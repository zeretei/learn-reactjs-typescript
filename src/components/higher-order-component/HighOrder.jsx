import HOCComment from "./Comment";
import HOCLike from "./Like";

const HighOrder = () => {
  return (
    <div>
      <HOCComment />
      <HOCLike />
    </div>
  );
};

export default HighOrder;
