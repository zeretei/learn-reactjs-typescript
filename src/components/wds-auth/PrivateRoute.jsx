import { Route, Redirect } from "react-router-dom";
import { useAuth } from "./AuthContext";

/**
 * Create a custom Route
 */
const PrivateRoute = ({ component: Component, ...rest }) => {
  const { authUser } = useAuth();

  return (
    <Route
      // rest of the props
      {...rest}
      render={(props) => {
        // check if the user is authenticated
        // return Component or redirect back to login
        return authUser ? <Component {...props} /> : <Redirect to="/login" />;
      }}
    ></Route>
  );
};

export default PrivateRoute;
