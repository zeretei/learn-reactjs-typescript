import { useState, useRef } from "react";
import { Link, useHistory } from "react-router-dom";

// import custom hook from created context
import { useAuth } from "./AuthContext";

// alert component
function Alert({ message }) {
  return (
    <div className="mb-4 border-l-4 border-red-500 text-red-900 bg-red-100 rounded px-4 py-3">
      <p className="text-sm text-red-700">{message}</p>
    </div>
  );
}

export default function UpdateProfile() {
  // useState
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  // use react-router-dom history hook
  const history = useHistory();

  // useRef
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmationRef = useRef();

  // const value = {signUp, authUser}
  const { authUser, updateEmail, updatePassword } = useAuth();

  // handle form submit
  const handleSubmit = async (e) => {
    // prevent event default behavior
    e.preventDefault();

    setError(null);
    setLoading(true);

    // create an object for inputs value
    const input = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
      passwordConfirmation: passwordConfirmationRef.current.value,
    };

    // check if password and confirm password match
    if (input.password !== input.passwordConfirmation) {
      return setError("Password and Confirm password didn't match.");
    }

    const promises = [];

    console.log(input.password);
    if (input.password) {
      promises.push(updatePassword(input.password));
    }

    if (input.email && input.email !== authUser.email) {
      promises.push(updateEmail(input.email));
    } else {
      return setError("New email is the same as old email");
    }

    Promise.all(promises)
      .then(() => {
        history.push("/dashboard");
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
        setError("failed to update profile");
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <div className="w-1/3 mx-auto p-6 shadow rounded">
      {/* on form submit call 'handlesubmit function' */}
      <form onSubmit={handleSubmit}>
        <h2 className="mb-6 text-blue-400 text-4xl text-center font-bold">
          Update Profile
        </h2>

        {/* check for errors */}
        {error && <Alert message={error} />}

        <div className="mb-4">
          <label className="block mb-1 text-sm font-bold text-gray-700">
            New Email
          </label>
          <input
            // reference input
            ref={emailRef}
            type="email"
            placeholder="Email Address..."
            className="w-full p-2 text-sm border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block mb-1 text-sm font-bold text-gray-700">
            New Password
          </label>
          <input
            // reference input
            ref={passwordRef}
            type="password"
            placeholder="Password..."
            className="w-full p-2 text-sm border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block mb-1 text-sm font-bold text-gray-700">
            Password Confirm
          </label>
          <input
            // reference input
            ref={passwordConfirmationRef}
            type="password"
            placeholder="Confirm Password..."
            className="w-full p-2 text-sm border rounded"
          />
        </div>
        <div className="mb-2 flex justify-between">
          <Link
            to="/dashboard"
            // disable button if the request is still loading
            disabled={loading}
            className="py-2 px-3 bg-red-400 text-white font-bold rounded hover:bg-red-500"
          >
            Cancel
          </Link>
          <button
            // disable button if the request is still loading
            disabled={loading}
            className="py-2 px-6 bg-blue-400 text-white font-bold rounded hover:bg-blue-500"
          >
            Update
          </button>
        </div>
      </form>
    </div>
  );
}
