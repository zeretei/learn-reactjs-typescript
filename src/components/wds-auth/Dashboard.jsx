import { useState } from "react";
import { useAuth } from "./AuthContext";
import { Link, useHistory } from "react-router-dom";

// alert component
const Alert = ({ message }) => {
  return (
    <div className="mb-4 border-l-4 border-red-500 text-red-900 bg-red-100 rounded px-4 py-3">
      <p className="text-sm text-red-700">{message}</p>
    </div>
  );
};

const Dashboard = () => {
  // set error state
  const [error, setError] = useState(null);

  // use 'useAuth' hook
  const { authUser, signOut } = useAuth();

  // use history from react-router-dom
  const history = useHistory();

  const handleLogout = async () => {
    try {
      // clear errors
      setError(null);

      // sign out user
      await signOut();

      // redirect to login
      history.push("/login");
    } catch (error) {
      setError("Failed to logout");
    }
  };

  return (
    <div className="w-1/3 p-4 mx-auto shadow rounded">
      {error && <Alert message={error} />}
      <div>
        <h2 className="mb-6 text-4xl font-bold text-center">Dashboard</h2>
        <p className="mb-4">
          Email: <span>{authUser.email}</span>
        </p>

        <div className="mb-4">
          <Link
            to="/update-profile"
            className="block py-2 px-3 bg-blue-400 text-white text-center rounded"
          >
            Update profile
          </Link>
        </div>
      </div>
      <div>
        <button
          onClick={handleLogout}
          className="py-2 px-3 text-white bg-red-400 rounded"
        >
          Logout
        </button>
      </div>
    </div>
  );
};

export default Dashboard;
