// import firebase app
import firebase from "firebase/app";

// import firebase auth
import "firebase/auth";

// import config
import { firebaseConfig } from "./../../.config";

// initialize firebase app
const app = firebase.initializeApp({
  // set config
  apiKey: firebaseConfig.apiKey,
  authDomain: firebaseConfig.authDomain,
  projectId: firebaseConfig.projectId,
  storageBucket: firebaseConfig.storageBucket,
  messagingSenderId: firebaseConfig.messagingSenderId,
  appId: firebaseConfig.appId,
});

// authentication instance
export const auth = app.auth();

// export firebase
export default app;
