import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import AuthProvider from "./AuthContext";

// import custom router
import PrivateRoute from "./PrivateRoute";

// components
import SignUp from "./SignUp";
import LogIn from "./LogIn";
import Dashboard from "./Dashboard";
import ForgotPassword from "./ForgotPassword";
import UpdateProfile from "./UpdateProfile";

const WdsAuth = () => {
  return (
    <Router>
      {/* // AuthProvider component */}
      <AuthProvider>
        <Switch>
          <Route exact path="/">
            <h2>Welcome page</h2>
          </Route>
          <PrivateRoute exact path="/dashboard" component={Dashboard} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/login" component={LogIn} />
          <Route exact path="/forgot-password" component={ForgotPassword} />
          <Route exact path="/update-profile" component={UpdateProfile} />
        </Switch>
      </AuthProvider>
    </Router>
  );
};

export default WdsAuth;
