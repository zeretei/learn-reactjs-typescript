import { useState, useRef } from "react";
import { Link } from "react-router-dom";

// import custom hook from created context
import { useAuth } from "./AuthContext";

// alert component
const Alert = ({ message }) => {
  return (
    <div className="mb-4 border-l-4 border-red-500 text-red-900 bg-red-100 rounded px-4 py-3">
      <p className="text-sm text-red-700">{message}</p>
    </div>
  );
};

// Success component
const Success = ({ message }) => {
  return (
    <div class="mb-4 border-l-4 border-green-500 text-teal-900 bg-green-100 rounded px-4 py-3">
      <p class="text-sm text-teal-700">{message}</p>
    </div>
  );
};
const SignIn = () => {
  // state
  const [error, setError] = useState(null);
  const [message, setMessage] = useState(null);
  const [loading, setLoading] = useState(false);
  const { forgotPassword } = useAuth();

  // ref
  const emailRef = useRef();

  // handle form submit
  const handleSubmit = async (e) => {
    // prevent event default behavior
    e.preventDefault();

    try {
      // set error state to null
      setError(null);
      // set loading state to true
      setLoading(true);

      // send a reset password to email
      await forgotPassword(emailRef.current.value);

      // set success message
      setMessage("Check your mail inbox");
    } catch (error) {
      setMessage(null);
      setError("Failed to reset password");
    }

    // set loading state to false
    setLoading(false);
  };

  return (
    <div className="w-1/3 mx-auto p-6 shadow rounded">
      {/* on form submit call 'handlesubmit function' */}
      <form onSubmit={handleSubmit}>
        <h2 className="mb-6 text-blue-400 text-4xl text-center font-bold">
          Forgot Password
        </h2>

        {/* check for errors */}
        {error && <Alert message={error} />}

        {/* success message */}
        {message && <Success message={message} />}

        {/* output authenticated user email */}
        {/* {authUser && authUser.email} */}

        <div className="mb-4">
          <label className="block mb-1 text-sm font-bold text-gray-700">
            Email
          </label>
          <input
            // reference input
            ref={emailRef}
            type="email"
            placeholder="Email Address..."
            className="w-full p-2 text-sm border rounded"
          />
        </div>
        <div className="mb-2">
          <button
            // disable button if the request is still loading
            disabled={loading}
            className="w-full py-2 px-3 bg-blue-400 text-white font-bold rounded hover:bg-blue-500"
          >
            Forgot Password
          </button>
        </div>

        {/* redirect to signup page */}
        <div className="mb-4">
          <p className="text-sm">
            Need an account?{" "}
            <Link to="/signup" className="text-blue-700">
              Signup
            </Link>
          </p>
        </div>
      </form>
    </div>
  );
};

export default SignIn;
