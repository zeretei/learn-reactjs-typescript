import React, { useContext, useState, useEffect } from "react";

// import initialized firebase
import { auth } from "./firebase";

// create a context
const AuthContext = React.createContext();

// custom hook to access created context
export function useAuth() {
  return useContext(AuthContext);
}

// create a context provider
export default function AuthProvider({ children }) {
  // create states
  const [authUser, setAuthUser] = useState(null);
  const [loading, setLoading] = useState(true);

  // check for auth state changes
  useEffect(() => {
    const subscribe = auth.onAuthStateChanged((user) => {
      // set authenticated user
      user && setAuthUser(user);

      // set loading state to false
      setLoading(false);
    });

    return subscribe;
  }, [authUser]);

  // create a firebase user
  const signUp = (email, password) => {
    return auth.createUserWithEmailAndPassword(email, password);
  };

  // signin to firebase
  const signIn = (email, password) => {
    return auth.signInWithEmailAndPassword(email, password);
  };
  // sign out from firebase
  const signOut = () => {
    return auth.signOut();
  };

  // reset password
  const forgotPassword = (email) => {
    return auth.sendPasswordResetEmail(email);
  };

  const updateEmail = (email) => {
    return authUser.updateEmail(email);
  };
  const updatePassword = (password) => {
    console.log(password);
    return authUser.updatePassword(password);
  };

  // set provider value prop
  const value = {
    authUser,
    signUp,
    signIn,
    signOut,
    forgotPassword,
    updateEmail,
    updatePassword,
  };

  return (
    // use created context
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
}
