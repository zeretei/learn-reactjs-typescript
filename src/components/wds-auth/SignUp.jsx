import { useState, useRef } from "react";
import { Link, useHistory } from "react-router-dom";

// import custom hook from created context
import { useAuth } from "./AuthContext";

// alert component
const Alert = ({ message }) => {
  return (
    <div className="mb-4 border-l-4 border-red-500 text-red-900 bg-red-100 rounded px-4 py-3">
      <p className="text-sm text-red-700">{message}</p>
    </div>
  );
};

const SignUp = () => {
  // useState
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  // use react-router-dom history hook
  const history = useHistory();

  // useRef
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmationRef = useRef();

  // const value = {signUp, authUser}
  const { signUp, authUser } = useAuth();

  // handle form submit
  const handleSubmit = async (e) => {
    // prevent event default behavior
    e.preventDefault();

    // create an object for inputs value
    const input = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
      passwordConfirmation: passwordConfirmationRef.current.value,
    };

    // check if password and confirm password match
    if (input.password !== input.passwordConfirmation) {
      return setError("Password and Confirm password didn't match.");
    }

    try {
      // set error state to null
      setError(null);
      // set loading state to true
      setLoading(true);

      // sign up user
      await signUp(emailRef.current.value, passwordRef.current.value);

      // set loading state to false
      setLoading(false);

      // redirect to sign in page
      history.push("/login");
    } catch (error) {
      // catch if signUp() fails
      setError("Failed to create an account");
    }
  };

  return (
    <div className="w-1/3 mx-auto p-6 shadow rounded">
      {/* on form submit call 'handlesubmit function' */}
      <form onSubmit={handleSubmit}>
        <h2 className="mb-6 text-blue-400 text-4xl text-center font-bold">
          Sign Up
        </h2>

        {/* check for errors */}
        {error && <Alert message={error} />}

        {/* output authenticated user email */}
        {authUser && authUser.email}

        <div className="mb-4">
          <label className="block mb-1 text-sm font-bold text-gray-700">
            Email
          </label>
          <input
            // reference input
            ref={emailRef}
            type="email"
            placeholder="Email Address..."
            className="w-full p-2 text-sm border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block mb-1 text-sm font-bold text-gray-700">
            Password
          </label>
          <input
            // reference input
            ref={passwordRef}
            type="password"
            placeholder="Password..."
            className="w-full p-2 text-sm border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block mb-1 text-sm font-bold text-gray-700">
            Password Confirm
          </label>
          <input
            // reference input
            ref={passwordConfirmationRef}
            type="password"
            placeholder="Confirm Password..."
            className="w-full p-2 text-sm border rounded"
          />
        </div>
        <div className="mb-2">
          <button
            // disable button if the request is still loading
            disabled={loading}
            className="w-full py-2 px-3 bg-blue-400 text-white font-bold rounded hover:bg-blue-500"
          >
            Sign Up
          </button>
        </div>
        <div className="mb-4">
          <p className="text-sm">
            Already have an account?
            <Link to="/login" className="text-blue-700">
              Login
            </Link>
          </p>
        </div>
      </form>
    </div>
  );
};

export default SignUp;
