import Button from "@material-ui/core/Button";

// interface
import { CartItemType } from "./Entree";

// Styles
import { Wrapper } from "./css/Item.styles";

// set interface for props
interface Props {
  item: CartItemType;
  handleAddToCart: (item: CartItemType) => void;
}

// react functional component
const Item: React.FC<Props> = ({ item, handleAddToCart }) => {
  return (
    <Wrapper>
      <img src={item.image} alt={item.title} />
      <div className="item">
        <div>
          <h3>{item.title}</h3>
          <p>{item.description}</p>
        </div>
        <h3 className="price">${item.price}</h3>
      </div>
      <Button onClick={() => handleAddToCart(item)}>Add to Cart</Button>
    </Wrapper>
  );
};

export default Item;
