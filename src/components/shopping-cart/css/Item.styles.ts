import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  width: 100%;
  height: 100%;
  border: 1px solid rgba(173, 216, 230, 1);
  border-radius: 20px;

  button {
    border-radius: 0 0 20px 20px;
    background: rgba(173, 216, 230, 1);
  }

  img {
    max-height: 250px;
    object-fit: cover;
    border-radius: 20px 20px 0 0;
  }

  div {
    font-family: arial;
    height: 100%;
  }

  h3 {
    font-weight: bold;
  }

  p {
    margin: 10px 0;
    font-size: 0.8rem;
  }

  .item {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: 1rem;
  }
`;
