import { QueryClient, QueryClientProvider } from "react-query";
import Entree from "./Entree";

const ShoppingCart = () => {
  const client = new QueryClient();

  return (
     // use react query
    <QueryClientProvider client={client}>
      <Entree />
    </QueryClientProvider>
  );
};

export default ShoppingCart;
