import CartItem from "./ CartItem";

// styles
import { Wrapper } from "./css/Cart.styles";

// interface
import { CartItemType } from "./Entree";

// set interface for props
interface Props {
  cartItems: CartItemType[];
  addToCart: (item: CartItemType) => void;
  removeFromCart: (id: number) => void;
}

const Cart: React.FC<Props> = ({ cartItems, addToCart, removeFromCart }) => {
  // get total items on cart
  const calculateTotal = (items: CartItemType[]) =>
    items.reduce((ack: number, item) => ack + item.amount * item.price, 0);

    console.log(calculateTotal);

  return (
    <Wrapper>
      <h2>Your Shopping Cart</h2>
      {cartItems.length === 0 ? <p>No items in cart.</p> : null}
      {cartItems.map((item) => (
        <div>
          <CartItem
            key={item.id}
            item={item}
            addToCart={addToCart}
            removeFromCart={removeFromCart}
          />
          <h2>Total: ${calculateTotal(cartItems).toFixed(2)}</h2>
        </div>
      ))}
    </Wrapper>
  );
};

export default Cart;
