import { useState } from "react";
import { useQuery } from "react-query";

// components
import Drawer from "@material-ui/core/Drawer";
import LinearProgress from "@material-ui/core/LinearProgress";
import Grid from "@material-ui/core/Grid";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import Badge from "@material-ui/core/Badge";

import Item from "./Item";
import Cart from "./ Cart";

// styles
import { Wrapper, StyledButton } from "./css/Entree.styles";

// interface
export interface CartItemType {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  title: string;
  amount: number;
}

// fetch all products
const getProducts = async (): Promise<CartItemType[]> =>
  // return as json
  await (await fetch("https://fakestoreapi.com/products")).json();

const Entree = () => {
  // set cart component state
  const [cartIsOpen, setCartIsOpen] = useState(false);
  // set cart items state
  const [cartItems, setCartItems] = useState([] as CartItemType[]);

  // use react-query
  const { data, isLoading, error } = useQuery<CartItemType[]>(
    "prodcts",
    getProducts
  );

  // get total items added
  const getTotalItems = (items: CartItemType[]) =>
    items.reduce((ack: number, item) => ack + item.amount, 0);

  // add an item to cart
  const handleAddToCart = (item: CartItemType) => {
    // prev: previous state
    setCartItems((prev) => {
      // check if the item is in the cart
      const inCart = prev.find((prevItem) => prevItem.id === item.id);

      if (inCart) {
        // incement amount
        return prev.map((prevItem) =>
          prevItem.id === item.id
            ? { ...prevItem, amount: prevItem.amount + 1 }
            : prevItem
        );
      }
      // add item to cart
      return [...prev, { ...item, amount: 1 }];
    });
  };

  // remove an item from cart
  const handleRemoveFromCart = (id: number) => {
    setCartItems((prev) =>
      prev.reduce((ack, prevItem) => {
        // check if in cart
        if (prevItem.id === id) {
          // remove the item cart
          if (prevItem.amount === 1) return ack;

          // decrement item
          return [...ack, { ...prevItem, amount: prevItem.amount - 1 }];

          // return ack and item
        } else {
          return [...ack, prevItem];
        }
      }, [] as CartItemType[])
    );
  };

  // show loading state
  if (isLoading) return <LinearProgress />;

  // check react-query errors
  if (error) return <p>something went wrong.</p>;
  return (
    <Wrapper>
      {/* cart sidebar component */}
      <Drawer
        anchor="right"
        open={cartIsOpen}
        onClose={() => setCartIsOpen(false)}
      >
        <Cart
          cartItems={cartItems}
          addToCart={handleAddToCart}
          removeFromCart={handleRemoveFromCart}
        />
      </Drawer>

      {/* cart icon */}
      <StyledButton onClick={() => setCartIsOpen(true)}>
        <Badge badgeContent={getTotalItems(cartItems)} color="error">
          <AddShoppingCartIcon />
        </Badge>
      </StyledButton>

      {/* all shopping items */}
      <Grid container spacing={3}>
        {data?.map((item) => (
          <Grid item key={item.id} xs={12} sm={4}>
            <Item item={item} handleAddToCart={handleAddToCart} />
          </Grid>
        ))}
      </Grid>
    </Wrapper>
  );
};

export default Entree;
