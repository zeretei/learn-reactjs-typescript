import React from "react";
import Todo from "./Todo";
const Todos = () => {
  const todos = [
    { id: 1, title: "title 1", completed: false },
    { id: 2, title: "title 2", completed: true },
    { id: 3, title: "title 3", completed: false },
  ];

  return (
    <div data-testid="todos">
      {todos.map((todo) => (
        <Todo
          key={todo.id}
          id={todo.id}
          title={todo.title}
          completed={todo.completed}
        />
      ))}
    </div>
  );
};

export default Todos;
