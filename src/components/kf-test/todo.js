const Todo = ({ id, title, completed }) => {
  return (
    <div data-testid={`todo-${id}`}>
      <h2 className={completed ? "line-through" : ""}>{title}</h2>
    </div>
  );
};

export default Todo;
