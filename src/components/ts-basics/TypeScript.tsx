import { useState, useRef } from "react";

// create an interface
interface Person {
  // property: type
  firstName: string;

  // '?': null 
  lastName?: string;
}

interface Props {
  // string
  text: string;

  // boolean or null
  ok?: boolean;

  // numeric
  i?: number;

  // function
  fn?: (bob: string) => string;

  // object
  obj?: Person;
}

// React.FC: react file component
// <Props>: set type
const TypeScript: React.FC<Props> = () => {
  // <number | null>: accept number or null value
  const [count, setCount] = useState<number | null>(5);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const divRef = useRef<HTMLDivElement | null>(null);

  //   setCount(7);
  console.log(count);

  return (
    <div ref={divRef}>
      <h2>typescript component</h2>
      <div>
        <label className="block">input</label>
        <input type="text" ref={inputRef} className="border p-2 rounded" />
      </div>
    </div>
  );
};

export default TypeScript;
