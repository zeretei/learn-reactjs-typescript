/**
 * Test a value
 */
expect(value)
expect(true).toBe(true) // pass
expect(true).toBe(false) // fail

/**
 * Create a custom matcher
 */
expect.extend(matchers)
expect.extend({
  // keyword
  inArray(haystack, neddle) {
    const pass = haystack.includes(neddle)

    // check if the test pass
    if (pass) {
      // return the message and result
      return {
        message: () => `${needle} exists in ${haystack}`,
        pass: true,
      }

    // test fails
    } else {
      // return the message and result
      return {
        message: () => `${needle} doesn't exists in ${haystack}`,
        pass: false,
      }
    }
  }
})

// call custom matcher
expect([1,2,3]).inArray(1);

/**
 * Match anything except null or undifined
 */
expect.anything()
expect(true).toEqual(expect.anything()) // pass
expect(null).toEqual(expect.anything()) // fail

/**
 *
 */
expect.any(constructor)
/**
 *
 */
expect.arrayContaining(array)
/**
 *
 */
expect.assertions(number)
/**
 *
 */
expect.hasAssertions()
/**
 *
 */
expect.not.arrayContaining(array)
/**
 *
 */
expect.not.objectContaining(object)
/**
 *
 */
expect.not.stringContaining(string)
/**
 *
 */
expect.not.stringMatching(string | regexp)
/**
 *
 */
expect.objectContaining(object)
/**
 *
 */
expect.stringContaining(string)
/**
 *
 */
expect.stringMatching(string | regexp)
/**
 *
 */
expect.addSnapshotSerializer(serializer)
/**
 *
 */
.not
/**
 *
 */
.resolves
/**
 *
 */
.rejects
/**
 *
 */
.toBe(value)
/**
 *
 */
.toHaveBeenCalled()
/**
 *
 */
.toHaveBeenCalledTimes(number)
/**
 *
 */
.toHaveBeenCalledWith(arg1, arg2, ...)
/**
 *
 */
.toHaveBeenLastCalledWith(arg1, arg2, ...)
/**
 *
 */
.toHaveBeenNthCalledWith(nthCall, arg1, arg2, ....)
/**
 *
 */
.toHaveReturned()
/**
 *
 */
.toHaveReturnedTimes(number)
/**
 *
 */
.toHaveReturnedWith(value)
/**
 *
 */
.toHaveLastReturnedWith(value)
/**
 *
 */
.toHaveNthReturnedWith(nthCall, value)
/**
 *
 */
.toHaveLength(number)
/**
 *
 */
.toHaveProperty(keyPath, value?)
/**
 *
 */
.toBeCloseTo(number, numDigits?)
/**
 *
 */
.toBeDefined()
/**
 *
 */
.toBeFalsy()
/**
 *
 */
.toBeGreaterThan(number | bigint)
/**
 *
 */
.toBeGreaterThanOrEqual(number | bigint)
/**
 *
 */
.toBeLessThan(number | bigint)
/**
 *
 */
.toBeLessThanOrEqual(number | bigint)
/**
 *
 */
.toBeInstanceOf(Class)
/**
 *
 */
.toBeNull()
/**
 *
 */
.toBeTruthy()
/**
 *
 */
.toBeUndefined()
/**
 *
 */
.toBeNaN()
/**
 *
 */
.toContain(item)
/**
 *
 */
.toContainEqual(item)
/**
 *
 */
.toEqual(value)
/**
 *
 */
.toMatch(regexp | string)
/**
 *
 */
.toMatchObject(object)
/**
 *
 */
.toMatchSnapshot(propertyMatchers?, hint?)
/**
 *
 */
.toMatchInlineSnapshot(propertyMatchers?, inlineSnapshot)
/**
 *
 */
.toStrictEqual(value)
/**
 *
 */
.toThrow(error?)
/**
 *
 */
.toThrowErrorMatchingSnapshot(hint?)
/**
 *
 */
.toThrowErrorMatchingInlineSnapshot(inlineSnapshot))
