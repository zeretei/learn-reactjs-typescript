/* BAD */
.btn {
  transform: scale(1.5) rotate(45deg)
}

.scale {
  transform: scale(2) rotate(45deg)
}

.rotate { transform: scale(1.5) rotate(90deg) }

/* GOOD */
.btn {
  transform: scale(var(--scale, 1.5)) rotate(var(--rotation, 45deg))
}

.scale {
  --scale: 2;
}

.rotate {
  --rotation: 90deg;
}
